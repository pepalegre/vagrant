###################################################################################################
"dev": Nou front-end de desenvolupament (Vagrant)
###################################################################################################

Configuració bàsica

- Posar l'usuari i el password al fitxer "dev/credentials.txt" (salts de linia format UNIX: "\n")
- Posar la ruta de la carpeta compartida on es troben els projectes SENSE EL HOSTNAME
- Si s'eliminen els 2 fitxers anteriors el sistema no intentarà muntar el "/www"

Comandes vagrant

( afegir '--debug' para visualitzar més detallat)

- vagrant up ( arrenca la màquina )
- vagrant halt ( atura la màquina )
- vagrant reload ( reiniciar la màquina )
- vagrant reload --provision ( reiniciar la màquina rellegint el fitxer vagrantFile )

Enllaços d'interès

 - Connectar a la màquina virtual a través de Putty
   https://github.com/Varying-Vagrant-Vagrants/VVV/wiki/Connect-to-Your-Vagrant-Virtual-Machine-with-PuTTY

Notes

  - En la màquina virtual teniu instal.lat

        a. Un servidor apache amb un virtualhost al port 80 que apunta a "/www/polaris/public"
           Per comprovar-ho des de fora la màquina virtual accedir a: "http://127.0.0.1" o "https://127.0.0.1"

        b. Un servidor MySQL. Les credencials per entrar son: "root" i "123+-"
           S'han creat un parell de perfils i alias pels usuaris "vagrant" i "root":
           - "mysql --login-path=local" (alias "mysql_local"): MySQL local (el del vagrant)
           - "mysql --login-path=remote" (alias "mysql_remote"): MySQL backend de desenvolupament (192.168.0.17)

        c. Si es volen tenir més ports oberts accessibles des de la nostra màquina s'ha fer "forward" al "Vagrantfile":
           config.vm.network "forwarded_port", guest: <port màquina virtual>, host: <port màquina local>, host_ip: "127.0.0.1"
