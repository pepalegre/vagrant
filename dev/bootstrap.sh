#!/bin/bash

# ----------------------------------------------------------------------------------------------------------------------
# Habilitem "root"
# ----------------------------------------------------------------------------------------------------------------------
sudo su

# ----------------------------------------------------------------------------------------------------------------------
# Deshabilitar SELINUX
# (https://github.com/phstudy/vagrant-hadoop-etu/blob/master/bash/disable-selinux.sh)
# ----------------------------------------------------------------------------------------------------------------------
echo "#########################################################################################"
echo "DESHABILITANT SELINUX ..."
echo "#########################################################################################"
setenforce 0
sed -i 's/SELINUX=\(enforcing\|permissive\)/SELINUX=disabled/' /etc/selinux/config

# ----------------------------------------------------------------------------------------------------------------------
# Firewall
# ----------------------------------------------------------------------------------------------------------------------
echo "#########################################################################################"
echo "DESHABILITANT FIREWALL ..."
echo "#########################################################################################"
systemctl stop firewalld
systemctl disable firewalld

# ----------------------------------------------------------------------------------------------------------------------
# Repositoris EPEL i REMI
# ----------------------------------------------------------------------------------------------------------------------
echo "#########################################################################################"
echo "INSTAL.LANT REPOSITORIS EPEL I REMI ..."
echo "#########################################################################################"
yum -y update
yum -y install epel-release.noarch
yum-config-manager -y --enable epel
yum -y install https://rpms.remirepo.net/enterprise/remi-release-7.rpm
yum-config-manager -y --enable remi remi-php71
yum -y update

# ----------------------------------------------------------------------------------------------------------------------
# Utilitats
# ----------------------------------------------------------------------------------------------------------------------
echo "#########################################################################################"
echo "INSTAL.LANT PAQUETS BASE DEL CENTOS ..."
echo "#########################################################################################"
yum -y groupinstall "Base" --setopt=group_package_types=mandatory,default,optional
yum -y install coreutils expect mc

# ----------------------------------------------------------------------------------------------------------------------
# ODBC
# (https://docs.microsoft.com/es-es/sql/connect/odbc/linux-mac/installing-the-microsoft-odbc-driver-for-sql-server)
# ----------------------------------------------------------------------------------------------------------------------
echo "#########################################################################################"
echo "INSTAL.LANT ODBC DE MICROSOFT ..."
echo "#########################################################################################"
curl https://packages.microsoft.com/config/rhel/7/prod.repo > /etc/yum.repos.d/mssql-release.repo
yum -y remove unixODBC-utf16 unixODBC-utf16-devel
ACCEPT_EULA=Y yum -y install msodbcsql
ACCEPT_EULA=Y yum -y install mssql-tools
yum install -y unixODBC-devel

cat << \EOT >> /etc/profile.d/odbc.sh
#!/bin/bash
export PATH=$PATH:/opt/mssql-tools/bin
EOT

chmod +x /etc/profile.d/odbc.sh

# ----------------------------------------------------------------------------------------------------------------------
# Java
# (http://www.oracle.com/technetwork/java/javase/downloads/index.html)
# (https://www.unixmen.com/install-oracle-java-jdk-8-centos-76-56-4/)
# ----------------------------------------------------------------------------------------------------------------------
echo "#########################################################################################"
echo "INSTAL.LANT JAVA ..."
echo "#########################################################################################"
yum -y localinstall /vagrant/packages/jdk-8u162-linux-x64.rpm

cat << \EOT >> /etc/profile.d/java.sh
#!/bin/bash
export JAVA_HOME=/usr/java/latest
export PATH=$JAVA_HOME/bin:$PATH
export CLASSPATH=.
EOT

chmod +x /etc/profile.d/java.sh

# ----------------------------------------------------------------------------------------------------------------------
# MySQL
# ----------------------------------------------------------------------------------------------------------------------
echo "#########################################################################################"
echo "INSTAL.LANT MYSQL ..."
echo "#########################################################################################"
yum -y install https://dev.mysql.com/get/mysql57-community-release-el7-11.noarch.rpm

yum -y install \
  mysql-community-client mysql-community-common mysql-community-devel mysql-community-embedded \
  mysql-community-embedded-devel mysql-community-libs mysql-community-libs-compat \
  mysql-community-server mysql-connector-java.noarch

cat << \EOT >> /etc/my.cnf
character_set_server = utf8
collation_server = utf8_general_ci
explicit_defaults_for_timestamp = 1
EOT

systemctl start mysqld

pass=`awk '/temporary password/ {print $NF}' /var/log/mysqld.log`
mysql -uroot -p${pass} --connect-expired-password -e "ALTER USER 'root'@'localhost' IDENTIFIED BY 'Vagrant1+';"
mysql -uroot -pVagrant1+ -e "uninstall plugin validate_password"
mysqladmin -u root -pVagrant1+ password '123+-'

>/tmp/init_mysql.sh
chmod +x /tmp/init_mysql.sh

cat << \EOT > /tmp/init_mysql.sh
#!/usr/bin/expect -f
spawn mysql_config_editor set --login-path=local --user=root --password
expect "Enter password: "
send "123+-\r"
expect eof
EOT
/tmp/init_mysql.sh

cat << \EOT > /tmp/init_mysql.sh
#!/usr/bin/expect -f
spawn mysql_config_editor set --login-path=remote --host 192.168.0.17 --user=root --password
expect "Enter password: "
send "4915265535\r"
expect eof
EOT
/tmp/init_mysql.sh

rm -f /tmp/init_mysql.sh

echo "alias mysql_local='mysql --login-path=local'" >> /root/.bashrc
echo "alias mysql_remote='mysql --login-path=remote'" >> /root/.bashrc

cp /root/.mylogin.cnf /home/vagrant
chown vagrant:vagrant /home/vagrant/.mylogin.cnf

echo "alias mysql_local='mysql --login-path=local'" >> /home/vagrant/.bashrc
echo "alias mysql_remote='mysql --login-path=remote'" >> /home/vagrant/.bashrc

systemctl enable mysqld

# ----------------------------------------------------------------------------------------------------------------------
# Apache
# ----------------------------------------------------------------------------------------------------------------------
echo "#########################################################################################"
echo "INSTAL.LANT APACHE ..."
echo "#########################################################################################"
yum -y install httpd httpd-tools mod_ssl

sed -i 's/EnableSendfile on/EnableSendfile off/' /etc/httpd/conf/httpd.conf
sed -i '/#ServerName/a ServerName localhost.localdomain' /etc/httpd/conf/httpd.conf

mkdir -p /www/polaris/public

cat << \EOT >> /etc/httpd/conf.d/iametrics.conf
# Permissos 'www' local
<Directory "/www">
    Options Indexes FollowSymLinks
    AllowOverride All
    Require all granted
</Directory>

# Listen 80

<VirtualHost *:80>

    # Alias /IAI /www/IAI/public

    ExpiresActive On
    ExpiresDefault "access plus 1 year"
    FileETag MTime

    <Location />
        SetOutputFilter DEFLATE
        BrowserMatch ^Mozilla/4 gzip-only-text/html
        BrowserMatch ^Mozilla/4\.0[678] no-gzip
        BrowserMatch \bMSIE !no-gzip !gzip-only-text/html
        SetEnvIfNoCase Request_URI \
        \.(?:gif|jpe?g|png|flv|swf|pdf)$ no-gzip dont-vary
        Header append Vary User-Agent env=!dont-vary
    </Location>

    # SetEnv APPLICATION_ENV "dev"
    SetEnvIf APPLICATION_ENV  ^(.*)$ APPLICATION_ENV=dev

    ServerName localhost.localdomain
    ServerAdmin vagrant@iasist.com
    DocumentRoot "/www/polaris/public"
    ErrorLog /var/log/httpd/polaris_error_log
    LogFormat "%h %l %u %t \"%r\" %>s %b" common
    CustomLog /var/log/httpd/polaris_access_log common

</VirtualHost>
EOT

sed -i '/<VirtualHost/a SetEnvIf APPLICATION_ENV  ^(.*)$ APPLICATION_ENV=dev' /etc/httpd/conf.d/ssl.conf
sed -i '/#ServerName/a DocumentRoot "/www/polaris/public"' /etc/httpd/conf.d/ssl.conf

systemctl enable httpd

# ----------------------------------------------------------------------------------------------------------------------
# PHP
# ----------------------------------------------------------------------------------------------------------------------
echo "#########################################################################################"
echo "INSTAL.LANT PHP ..."
echo "#########################################################################################"
yum -y install \
  php php-cli php-common php-devel php-gd php-gmp php-json php-mbstring php-mcrypt php-mysqlnd php-pdo php-pdo-dblib \
  php-pecl-xdebug php-pecl-zip php-process php-soap php-sqlsrv php-tidy php-xml php-doctrine-instantiator.noarch \
  php-fedora-autoloader.noarch php-myclabs-deep-copy.noarch php-pear.noarch \
  php-phpdocumentor-reflection-docblock2.noarch php-phpspec-prophecy.noarch php-phpunit-* php-sebastian-* \
  libXrender libXext libXfont libXfont-devel libfontenc libfontenc-devel lyx-fonts.noarch xorg-x11-font-utils \
  xorg-x11-fonts-*

sed -i '/;browscap/a browscap = /etc/php_browscap.ini' /etc/php.ini
sed -i '/;error_log = php_errors.log/a error_log = /var/log/php_errors.log' /etc/php.ini

cat << \EOT >> /opt/actualitzar_browscap.sh
#!/bin/bash
curl -s http://browscap.org/stream?q=PHP_BrowsCapINI > /etc/php_browscap.ini
systemctl restart httpd
EOT

chmod 755 /opt/actualitzar_browscap.sh
/opt/actualitzar_browscap.sh

# ----------------------------------------------------------------------------------------------------------------------
# Composer
# ----------------------------------------------------------------------------------------------------------------------
echo "#########################################################################################"
echo "INSTAL.LANT COMPOSER ..."
echo "#########################################################################################"
php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');"
php composer-setup.php --install-dir=/usr/local/bin --filename=composer
php -r "unlink('composer-setup.php');"

# ----------------------------------------------------------------------------------------------------------------------
# Links (per compatibilitat amb Polaris, Dimoni GPS v3 Out, ...)
# ----------------------------------------------------------------------------------------------------------------------
echo "#########################################################################################"
echo "CREANT LINKS PER COMPATIBILITAT EN POLARIS ..."
echo "#########################################################################################"
mkdir -p /software/bin/mysql/bin
ln -s /usr/bin/mysql /software/bin/mysql/bin/mysql
ln -s /usr/bin/mysqldump /software/bin/mysql/bin/mysqldump
ln -s /usr/bin/mysqlimport /software/bin/mysql/bin/mysqlimport

mkdir -p /software/bin/php/bin
ln -s /usr/bin/php /software/bin/php/bin/php

mkdir -p /software/bin/freetds/bin
ln -s /usr/bin/tsql /software/bin/freetds/bin/tsql

# ----------------------------------------------------------------------------------------------------------------------
# APPLICATION_ENV
# ----------------------------------------------------------------------------------------------------------------------
echo "#########################################################################################"
echo "CREANT VARIABLE D'ENTORN APPLICATION_ENV ..."
echo "#########################################################################################"
cat << \EOT >> /etc/profile.d/entorno.sh
#!/bin/bash
APPLICATION_ENV=dev
export APPLICATION_ENV
EOT

chmod +x /etc/profile.d/entorno.sh

sed -i '/MAILTO=/a APPLICATION_ENV="dev"' /etc/crontab
sed -i '/command to be executed/a 0 10 * * 1 root /opt/actualitzar_browscap.sh' /etc/crontab
sed -i '/XAUTHORITY/a Defaults    env_keep += "APPLICATION_ENV"' /etc/sudoers

# ----------------------------------------------------------------------------------------------------------------------
# Punts de muntatge
# ----------------------------------------------------------------------------------------------------------------------
echo "#########################################################################################"
echo "INSTAL.LANT PUNTS DE MUNTATGE ..."
echo "#########################################################################################"
yum -y install cifs-utils

mkdir -p /opt/credentials

cat << \EOT >> /opt/credentials/vmgps301.txt
username=administrador
password=Iasist2014
EOT

cat << \EOT >> /opt/credentials/anubiscred.txt
username=administrador
password=Iasist2014
EOT

cat << \EOT >> /opt/credentials/msdrg.txt
username=administrador
password=Iasist2014
EOT

cat << \EOT >> /opt/credentials/agrupador_cgs_ap.txt
username=agruprodlocal
password=4gRup4D0rl0C4l2K15
EOT

cat << \EOT >> /opt/credentials/agrupador_cgs_ir_cred.txt
username=Administrador
password=Iasist2014
EOT

mkdir /mnt/vmgps302
mkdir /mnt/anubis
mkdir /mnt/ppd
mkdir /mnt/data
mkdir /mnt/msdrg
mkdir /mnt/agrupador_cgs_ap
mkdir /mnt/agrupador_cgs_ap16
mkdir /mnt/agrupador_cgs_ir

cat << \EOT >> /etc/fstab
//192.168.0.13/vmgps302/ /mnt/vmgps302/ cifs credentials=/opt/credentials/vmgps301.txt,dir_mode=0777,file_mode=0777,uid=root,gid=root,rw 0 0
//192.168.0.8/pub /mnt/anubis cifs credentials=/opt/credentials/anubiscred.txt,dir_mode=0775,file_mode=0664 0 0
//192.168.0.8/data /mnt/ppd cifs defaults,credentials=/opt/credentials/anubiscred.txt,uid=apache,gid=apache,rw 0 0
//192.168.0.8/data /mnt/data cifs defaults,credentials=/opt/credentials/anubiscred.txt,uid=apache,gid=apache,rw 0 0
//192.168.0.8/msdrg /mnt/msdrg cifs credentials=/opt/credentials/msdrg.txt,uid=apache,gid=apache,rw 0 0
//192.168.0.25/3MCGS /mnt/agrupador_cgs_ap cifs defaults,credentials=/opt/credentials/agrupador_cgs_ap.txt,uid=apache,gid=apache,rw 0 0
//192.168.0.27/3MCGS /mnt/agrupador_cgs_ap16 cifs defaults,credentials=/opt/credentials/agrupador_cgs_ap.txt,uid=apache,gid=apache,rw 0 0
//192.168.1.7/3MCGS /mnt/agrupador_cgs_ir cifs defaults,credentials=/opt/credentials/agrupador_cgs_ir_cred.txt,uid=apache,gid=apache,rw 0 0
EOT

# ----------------------------------------------------------------------------------------------------------------------
# Configuració carpeta de projectes
# ----------------------------------------------------------------------------------------------------------------------
echo "#########################################################################################"
echo "INSTAL.LANT CARPETA DE PROJECTES COMPARTIDA ..."
echo "#########################################################################################"

mkdir -p /opt/www
cat << \EOT >> /opt/www/www_start.sh
#!/bin/bash
if [ -f /vagrant/projects_folder.txt ]; then
  if [ -f /vagrant/credentials.txt ]; then
    servidor="//`netstat -rn | awk '/^0.0.0.0/ {print $2}'`"
    carpeta="`head -1 /vagrant/projects_folder.txt`"
    params="credentials=/vagrant/credentials.txt,uid=apache,gid=apache,dir_mode=0775,file_mode=0775,users,noauto,rw,exec"
    sudo mount -t cifs ${servidor}${carpeta} /www -o ${params}
  fi
fi
EOT
chmod 744 /opt/www/www_start.sh

cat << \EOT >> /opt/www/www_stop.sh
#!/bin/bash
umount /www
EOT
chmod 744 /opt/www/www_stop.sh

cat << \EOT >> /etc/systemd/system/www.service
[Unit]
After=network.target

[Service]
Type=oneshot
ExecStart=/opt/www/www_start.sh
RemainAfterExit=true
ExecStop=/opt/www/www_stop.sh
StandardOutput=journal

[Install]
WantedBy=default.target
EOT
chmod 664 /etc/systemd/system/www.service
systemctl daemon-reload
systemctl enable www.service

# ----------------------------------------------------------------------------------------------------------------------
# Rebotem la màquina
# ----------------------------------------------------------------------------------------------------------------------
init 6
